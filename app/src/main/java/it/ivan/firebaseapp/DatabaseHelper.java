package it.ivan.firebaseapp;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class DatabaseHelper {
  private static DatabaseHelper sInstance;
  private FirebaseDatabase database;
  private DatabaseReference databaseReference;
  private static final String TAG = "DatabaseHelper";

  public static DatabaseHelper getInstance() {
    if (sInstance == null) {
      sInstance = new DatabaseHelper();
    }
    return sInstance;
  }

  private DatabaseHelper() {
    database = FirebaseDatabase.getInstance();
  }

  public void readFromDatabase(String reference) {
    databaseReference=database.getReference(reference);
    databaseReference.addValueEventListener(new ValueEventListener() {
      @Override
      public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
        String value = dataSnapshot.getValue(String.class);
        Log.d(TAG, "Il valore è: " + value);
      }

      @Override
      public void onCancelled(@NonNull DatabaseError databaseError) {
        Log.d(TAG, "Impossibile leggere il messaggio.", databaseError.toException());
      }
    });
  }
}
