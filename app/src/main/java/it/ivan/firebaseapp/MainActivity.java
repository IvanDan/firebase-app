package it.ivan.firebaseapp;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class MainActivity extends AppCompatActivity {

  private FirebaseDatabase database;
  private DatabaseReference myRef;
  private static final String TAG = "MainActivity";
  private FirebaseAuth firebaseAuth;
  private EditText editTextEmail;
  private EditText editTextPassword;
  private TextView textView;
  private DatabaseHelper dbInstance;
  private Button buttonUser;
  private FirebaseUser currentUser;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    dbInstance = DatabaseHelper.getInstance();

    setContentView(R.layout.activity_main);
    firebaseAuth = FirebaseAuth.getInstance();
    database = FirebaseDatabase.getInstance();
    editTextEmail = findViewById(R.id.editTextEmail);
    editTextPassword = findViewById(R.id.editTextPassword);
    textView = findViewById(R.id.textView);
    buttonUser = findViewById(R.id.buttonUser);

    currentUser = firebaseAuth.getCurrentUser();
    buttonUser.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        dbInstance.readFromDatabase("user/" + currentUser.getUid());
      }
    });
  }

  @Override
  protected void onStart() {
    super.onStart();
    currentUser = firebaseAuth.getCurrentUser();
    updateUI(currentUser);
  }

  /**
   * Questo metodo crea un <code>Toast</code>
   *
   * @param mex il messaggio da mostrare nel <code>Toast</code>
   */
  private void makeToast(CharSequence mex) {
    Toast.makeText(MainActivity.this, mex.toString(), Toast.LENGTH_LONG).show();
  }

  public void checkInputEmailAndPassword(View v) {
    if (editTextEmail.getText().toString().isEmpty()) {
      makeToast(getText(R.string.email_missing));
    } else if (editTextPassword.getText().toString().isEmpty()) {
      makeToast(getText(R.string.password_missing));
    }
  }

  private void updateUI(FirebaseUser currentUser) {// TODO - da completare il metodo
  }

  public void writeOnDatabase(View view) {
    FirebaseUser user = firebaseAuth.getCurrentUser();
    if (user != null) {
      myRef = database.getReference("user/" + user.getUid());
      if (myRef.setValue("Ciao " + user.getEmail()).isSuccessful()) {
        Toast.makeText(MainActivity.this, getText(R.string.write_on_database_successful), Toast.LENGTH_LONG).show();
      }
    } else {
      Toast.makeText(MainActivity.this, getText(R.string.write_on_database_unsuccessful), Toast.LENGTH_LONG).show();
    }
  }

  private void readFromDatabase() {
    myRef.addValueEventListener(new ValueEventListener() {
      @Override
      public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
        String value = dataSnapshot.getValue(String.class);
        Log.d(TAG, "Il valore è: " + value);
      }

      @Override
      public void onCancelled(@NonNull DatabaseError databaseError) {
        Log.d(TAG, "Impossibile leggere il messaggio.", databaseError.toException());
      }
    });
  }

  /**
   * Questo metodo legge dagli <code>EditText</code> email e password e li registra con il metodo <code>createUserWithEmailAndPassword()</code> nel progetto di Firebase.
   * <p>
   * Inoltre avverte con i Toast se la registrazione avviene con successo oppure no.
   */
  public void signUp(View view) {
    String email = editTextEmail.getText().toString().trim();
    String password = editTextPassword.getText().toString();
    firebaseAuth.createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
              @Override
              public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                  Toast.makeText(MainActivity.this, getText(R.string.signup_successful), Toast.LENGTH_LONG).show();
                  FirebaseUser user = firebaseAuth.getCurrentUser();
                  updateUI(user);
                } else {
                  Toast.makeText(MainActivity.this, getText(R.string.signup_unsuccessful), Toast.LENGTH_LONG).show();
                  updateUI(null);
                }
              }
            });
  }

  /**
   * Questo metodo legge dagli <code>EditText<\code> email e password ed effettua il log in dell’utente con il metodo <code>signInWithEmailAndPassword</code>.
   * <p>
   * Inoltre avverte con i Toast se il log in avviene con successo oppure no.
   */
  public void signIn(View view) {
    String email = editTextEmail.getText().toString().trim();
    String password = editTextPassword.getText().toString();
    firebaseAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
      @Override
      public void onComplete(@NonNull Task<AuthResult> task) {
        if (task.isSuccessful()) {
          Toast.makeText(MainActivity.this, getText(R.string.signin_successful), Toast.LENGTH_LONG).show();
          FirebaseUser user = firebaseAuth.getCurrentUser();
          updateUI(user);
        } else {
          Toast.makeText(MainActivity.this, getText(R.string.signin_unsuccessful), Toast.LENGTH_LONG).show();
          updateUI(null);
        }
      }
    });
  }

  /**
   * Questo metodo stampa nella <code>TextView</code> dell'app i dati dell'utente loggato in quel momento
   */
  public void getUser(View view) {
    FirebaseUser user = firebaseAuth.getCurrentUser();
    if (user != null) {
      String name = user.getDisplayName();
      String email = user.getEmail();

      boolean emailVerified = user.isEmailVerified();

      String message = getText(R.string.name) + ": " + name + "\n" + getText(R.string.email) + ": " + email + "\n" + emailVerified;
      textView.setText(message);
    }
  }
}
